package com.ucompensar.edu.co.kubernetesexampleapi.application.port.in;

import com.ucompensar.edu.co.kubernetesexampleapi.application.dto.InfoDTO;

import java.sql.SQLException;

public interface InfoUseCase {
    InfoDTO getInfo() throws SQLException;
}
