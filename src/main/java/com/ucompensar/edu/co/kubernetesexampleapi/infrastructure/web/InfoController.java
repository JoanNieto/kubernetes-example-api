package com.ucompensar.edu.co.kubernetesexampleapi.infrastructure.web;

import com.ucompensar.edu.co.kubernetesexampleapi.application.dto.InfoDTO;
import com.ucompensar.edu.co.kubernetesexampleapi.application.port.in.InfoUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

@RestController
@RequestMapping("/info")
@Tag(name = "api-info", description = "API to get Info")
@RequiredArgsConstructor
public class InfoController {

    private final InfoUseCase infoUseCase;

    @GetMapping
    @Operation(summary = "Get Info", tags = {"api-info"})
    public InfoDTO getInfo() throws SQLException {
        return infoUseCase.getInfo();
    }
}
