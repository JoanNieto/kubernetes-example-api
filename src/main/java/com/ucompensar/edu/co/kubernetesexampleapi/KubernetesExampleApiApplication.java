package com.ucompensar.edu.co.kubernetesexampleapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KubernetesExampleApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(KubernetesExampleApiApplication.class, args);
    }

}
