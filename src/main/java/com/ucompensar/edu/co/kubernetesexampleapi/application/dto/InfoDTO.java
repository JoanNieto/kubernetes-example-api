package com.ucompensar.edu.co.kubernetesexampleapi.application.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InfoDTO {
    private String appName;
    private String version;
    private DbInfoDTO dbInfo;
}
