package com.ucompensar.edu.co.kubernetesexampleapi.application.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DbInfoDTO {
    private String status;
    private String error;
    private String dbName;
    private String dbVersion;
    private long responseTime;
}
