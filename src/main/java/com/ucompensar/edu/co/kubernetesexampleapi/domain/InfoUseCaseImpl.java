package com.ucompensar.edu.co.kubernetesexampleapi.domain;

import com.ucompensar.edu.co.kubernetesexampleapi.application.dto.DbInfoDTO;
import com.ucompensar.edu.co.kubernetesexampleapi.application.dto.InfoDTO;
import com.ucompensar.edu.co.kubernetesexampleapi.application.port.in.InfoUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

@Service
@RequiredArgsConstructor
public class InfoUseCaseImpl implements InfoUseCase {

    private final JdbcTemplate jdbcTemplate;

    @Value("${spring.application.name}")
    private String appName;

    @Value("${spring.datasource.url}")
    private String dbUrl;

    @Value("${spring.datasource.username}")
    private String dbUsername;

    @Value("${spring.datasource.password}")
    private String dbPassword;

    @Override
    public InfoDTO getInfo() throws SQLException {
        return InfoDTO.builder()
                .appName(appName)
                .version("1.0.0")
                .dbInfo(checkDB())
                .build();
    }


    private DbInfoDTO checkDB() throws SQLException {
        DataSource ds = createDataSource(dbUrl, dbUsername, dbPassword);
        Connection connection = ds.getConnection();
        DatabaseMetaData metaData = connection.getMetaData();
        String dbName = metaData.getDatabaseProductName();
        String dbVersion = metaData.getDatabaseProductVersion();
        long startTime = System.currentTimeMillis();
        jdbcTemplate.queryForObject("SELECT 1", Long.class);
        long endTime = System.currentTimeMillis();
        long responseTime = endTime - startTime;

        return DbInfoDTO.builder()
                .status("OK")
                .dbName(dbName)
                .dbVersion(dbVersion)
                .responseTime(responseTime)
                .build();
    }

    private DataSource createDataSource(String url, String username, String password) {
        return DataSourceBuilder.create()
                .url(url)
                .username(username)
                .password(password)
                .build();
    }

}
