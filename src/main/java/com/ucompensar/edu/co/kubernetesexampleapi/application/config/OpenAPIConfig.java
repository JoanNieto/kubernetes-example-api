package com.ucompensar.edu.co.kubernetesexampleapi.application.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("Backend Auth API")
                        .version("1.0.0")
                        .description("API to manage users and authentication")
                        .contact(new Contact()
                                .name("Joan Salomón Nieto")
                                .email("joansalomon@gmail.com")
                                .url("github.com/jsalonl")
                        )
                        .license(new License().name("Apache 2.0"))
                );
//                .addServersItem(new Server()
//                        .url("http://localhost:8080/api/v1").description("Local server")
//                )
//                .addServersItem(new Server()
//                        .url("https://santabarbara.devsystems.tech").description("Stage Server")
//                );
    }

}
