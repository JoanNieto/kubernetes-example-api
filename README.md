# Ejemplo de enrutamiento de contenedores

## Descripción

Este es un ejemplo de cómo enrutar tráfico a un contenedor en base a la URL de la petición.

## Requisitos

- [Docker](https://www.docker.com/products/docker-desktop)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Uso

1. Clonar el repositorio

   ```bash
   git clone https://gitlab.com/JoanNieto/kubernetes-example-api.git
   ```

2. Entrar al directorio del proyecto

   ```bash
   cd kubernetes-example-api
   ```

3. Crear la red

   ```bash
   docker network create traefik-net
   ```
   
4. Construir y levantar los contenedores

   ```bash
   docker-compose up -d --build
   ```

5. Probar la aplicación

   Acceder a http://app.localhost/swagger-ui/index.html