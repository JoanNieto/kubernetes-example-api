# From gradle build
FROM gradle:8 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle clean
RUN gradle build

FROM openjdk:17-jdk-slim-buster
EXPOSE 8080
COPY --from=build /home/gradle/src/build/libs/kubernetes-example-api-1.0.0.jar /kubernetes-example-api-1.0.0.jar
ENTRYPOINT ["java", "-jar", "/kubernetes-example-api-1.0.0.jar"]